const net = require('net')
const { HL7Client, createServer } = require('hl7v2')
const msg = require('./msg')
const consola = require('consola')

consola.level = 5 // https://github.com/nuxt/consola/blob/master/src/types.js
consola.trace('trace level logging enabled')

async function sendMessageToClient(client) {
  let m = msg.orderSample()
  consola.start('sending message to connected socket')
  consola.trace(m)

  await client.send(m)
  consola.success('message was sent')
}

async function run () {
  let count = 0
  let emptyQueue = true
  let server = createServer(async function (socket) {
    count++
    consola.info(`mTablet has connected to server (${count}x)...`)

    const client = new HL7Client(socket)
    client.on('message', resp => {
      consola.info('HL7 message received')
      consola.trace(msg.printable(resp.toHL7()))
    })
    client.on('close', resp => {
      consola.info('client connection closed')
    })
    client.on('error', resp => {
      consola.error('client error', resp)
    })
    // client._extSocket.on('data', resp => {
    //   consola.info('received data:', msg.printable(resp.toString()))
    // })

    if (!emptyQueue) {
      await sendMessageToClient(client)
    }
  })

  server.on('error', err => {
    consola.error('server error', err)
  })

  server.use('ORU^RO1^ORU_R01', (req, socket) => {
    consola.info('received ORU^RO1^ORU_R01 message')
    consola.trace(msg.printable(req.toHL7()))
    const ack = server._createACK(req, 'AA', '')
    consola.info('sending ACK message')
    consola.trace(msg.printable(ack.toHL7()))
    return ack
  })

  server.use((req) => {
    consola.warn('unhandled message (should not happen)')
    consola.log(msg.printable(req.toHL7()))
  })

  server.listen(1234)
  consola.ready('server listens for mTablet communication on port 1234')

  // interacion with server patient queue
  const readline = require('readline')

  function askQuestion (query) {
    const rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout,
    })
    return new Promise(resolve => rl.question(query, ans => {
      rl.close()
      resolve(ans)
    }))
  }

  while (true) {
    console.info('patient in server queue: ' + !emptyQueue)
    const ans = await askQuestion('Press ENTER to toggle patient in server queue, or write `quit<ENTER>` to quit')
    if (ans === '.' || ans === 'quit') process.exit()
    emptyQueue = !emptyQueue
    if (!emptyQueue && server._sockets.size) {
      let socket = [...server._sockets][0]
      await sendMessageToClient(new HL7Client(socket))
      emptyQueue = true
    }
  }
}


run()
