const { HL7Message } = require('hl7v2')
/**
 * Provides HL7 messages without to clutter the server code
 */

const msg1 = `MSH|^~&|SENDING APPLICATION NAME|SENDING FACILITY|DCM4CHE|PACS|20181207123100||ORM^O01^ORM_O01|MSG5213843|P|2.5|||AL|NE||UNICODE UTF-8||
PID|1||31360^^^MIRTH^PI|31360|PATIENT FAMILY NAME^PATIENT GIVENNAME^^^^^L||19010101000000|F||2106-3^White^CDCREC|STREET ADDRESS1A^^CITY^^1000^SLOVENIA^L||^PRN^PH^|||||^^^AHV^NNSUI||||N||||705||||N|N
PV1|1|O||R|||04748^ATTENDING DOCTOR FAMILY NAME^ATTENDING DOCTOR GIVENNAME^^|04748^REFFERING DOCTOR FAMILYNAME^^^|^^^^|CAR|||||||||2916544^^^MIRTH^VN|||||||||||||||||||||||||20171207123100|||||||V
ORC|NW|5213843^MIRTH_PLC|||SC||1^Once^^20171207123100^^R||20171207123100|||04748^ORDERING PROVIDER NAME^^^|||20171207123100||||
OBR|1|5213843^MIRTH_PLC||93000^Resting ECG^C4|R|20171207123100||||||||||04748^ORDERING PROVIDER NAME^ORDERING PROVIDER NAME^^||||||||EC|||1^Once^^20171207123100^^R||||^^|||^^^SPS||20171207123100||||||||^12-lead ECG^SCT2
OBX|1|NM|8302-2^Body height^LN||175|cm|||||F
OBX|2|NM|29463-7^Body weight^LN||75|kg|||||F`

const id = '7501160000'
const msg2 = `MSH|^~&|CURO|SENDING FACILITY|DCM4CHE|PACS|20190902000000||ORM^O01^ORM_O01|MSG5213843|P|2.5|||AL|NE||UTF-8||
PID|1||${id}^^^MIRTH^PI|${id}|John^Doo^^^^^L||19750101000000|M||2106-3^White^CDCREC|STREET ADDRESS1A^^CITY^^1000^SLOVENSKÁ REPUBLIKA^L||^PRN^PH^|||||^^^AHV^NNSUI||||N||||705||||N|N
PV1|1|O||R|||04748^Jaj^Bolíto^^|04748^MUDr. Jaj Bolíto^^^|^^^^|CAR|||||||||2916544^^^MIRTH^VN|||||||||||||||||||||||||20171207123100|||||||V
ORC|NW|5213843^MIRTH_PLC|||SC||1^Once^^20171207123100^^R||20171207123100|||04748^ORDERING PROVIDER NAME^^^|||20171207123100||||
OBR|1|5213843^MIRTH_PLC||93000^Resting ECG^C4|R|20171207123100||||||||||04748^ORDERING PROVIDER NAME^ORDERING PROVIDER NAME^^||||||||EC|||1^Once^^20171207123100^^R||||^^|||^^^SPS||20171207123100||||||||^12-lead ECG^SCT2
OBX|1|NM|8302-2^Body height^LN||175|cm|||||F
OBX|2|NM|29463-7^Body weight^LN||75|kg|||||F`

class Messages {
  orderSample() {
    return msg2
  }

  order() {
    let msg = new HL7Message({version: '2.5'})
    let msh = msg.add('MSH')
    msh.SendingApplication.value = 'CUROMD'
    msh.SendingFacility.value = 'CURO s.r.o.'
    msh.ReceivingApplication.value = 'mtablet'
    msh.DateTimeOfMessage.value = new Date()

    let pid = msg.add('PID') // see http://www.hl7.eu/refactored/segPID.html#1840
    pid.SetIdPid.value = Date.now()
    pid.PatientIdentifierList.value = id
    pid.SsnNumberPatient.value = id
    pid.PatientName.value = null
    pid.PatientName[0].GivenName.value = 'John'
    pid.PatientName[0].FamilyName.value = 'Doo'
    pid.PatientName[0].NameTypeCode.value = 'L'
    pid.DateTimeOfBirth.value = new Date('1975-01-01')
    pid.AdministrativeSex.value = 'M'
    pid.Race.value = null
    pid.Race[0].Identifier.value = '2106-3'
    pid.Race[0].Text.value = 'White'
    pid.Race[0].NameOfCodingSystem.value = 'CDCREC'

    let pv1 = msg.add('PV1' )
    pv1.SetIdPv1.value = ''
    pv1.PatientClass.value= ''
    pv1.AdmissionType.value= ''
    pv1.AttendingDoctor.value = null
    pv1.AttendingDoctor[0].IdNumber= ''
    pv1.AttendingDoctor[0].FamilyName= ''
    pv1.AttendingDoctor[0].GivenName= ''
    pv1.ReferringDoctor.value = null
    pv1.ReferringDoctor[0].IdNumber= ''
    pv1.ReferringDoctor[0].FamilyName= ''
    pv1.ReferringDoctor[0].GivenName= ''

    let orc = msg.add('ORC')
    orc.OrderControl.value = ''
    orc.PlacerOrderNumber.value = null
    orc.PlacerOrderNumber[0].EntityIdentifier.value = ''
    orc.PlacerOrderNumber[0].NamespaceId.value = ''
    orc.PlacerGroupNumber.value = ''

    let obr = msg.add('OBR')
    obr.SetIdObr.value = 1
    let obx = msg.add('OBX')
    obx.SetIdObx.value = 1

    obx = msg.add('OBX')
    obx.SetIdObx.value = 2

    return msg.toHL7()
  }

  printable (msg) {
    return msg.replace(/\r/g, '\n')
  }
}

module.exports = new Messages()
