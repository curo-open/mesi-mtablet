# mesi-mtablet

Reference implementation of server communicating with Mesi mTablet Work list application.

## Install

`yarn`

## Usage

* start server: `yarn server` in console
* login on mTablet
* start `Work list application`
* enter IP address of your PC
  * or long hold text List to change already registered IP address
* in console enter `quit` and ENTER to stop the server
* in console press ENTER to add patient to server queue
  * the patient will be immediately send to mTablet if it is sonnected
  * or it will send the patient next time mTablet connects to the server
* patient will appear in mTablet `Work list`
* open the patient on mTablet and perform EKG
* after you confirm `DONE` button
  * mTablet will send result to server (data are printed to console)
  * server sends ACK message
  * **mTablet hangs**  (after a while message is displayed)
 
**We need your help to find out what is wrong with our ACK message**

Here is actual communication log you would receive from the steps described above:

```hl7
ℹ received ORU^RO1^ORU_R01 message
› MSH|^~\&|^MTABMD|^MESI Ltd|^BIRPIS21|^SRC Infonet|20190902000000||ORU^RO1^ORU_R01|MSG5213843|P|2.5.1|||SU|SU||UNICODE UTF-8
PID|1|7501160000|7501160000||John~^Doo||19750101000000|M||^^MESICOD~^White|^^^^^SLOVENSKÁ REPUBLIKA~^^^^1000~^^CITY~STREET ADDRESS1A|||||||||||N||||705||||N|N
PV1|1|O||R|||^^Bolíto~^Jaj~04748|^^Bolíto~^Jaj~04748
ORC|NW|5213843^MIRTH_PLC||EI|SC||^^^^^R~^^^20171207123100~~1|||||^ORDERING PROVIDER NAME~04748
OBR|1|||93000^Resting ECG||20171207123100
OBX|0|ST|ECG.Meta.URL^Url to the measurements on mRECORDS^MESI_MRECORDS||https://mrecords.mesimedical.com/patient/98e818a2-5f51-4553-add8-1a5534ce34d6/measurement/0a40cd98-4096-46b7-8307-358612c6f33d/ecg|^^UCUM|||||F
OBX|1|ST|ECG.Meta.Interpretation^Automatic interpretation from the Glasgow University algorithm^MESI_MRECORDS||[D]--- Recording unsuitable for analysis - please repeat ---;[S]Analysis error|^^UCUM|||||F
OBX|2|NM|ECG.Interval.RR^RR interval^MESI_MRECORDS||0|ms^Milliseconds^UCUM|||||F
OBX|3|NM|ECG.Interval.PR^PR interval^MESI_MRECORDS||0|ms^Milliseconds^UCUM|||||F
OBX|4|NM|ECG.Interval.P^P interval^MESI_MRECORDS||0|ms^Milliseconds^UCUM|||||F
OBX|5|NM|ECG.Interval.QT^QT interval^MESI_MRECORDS||0|ms^Milliseconds^UCUM|||||F
OBX|6|NM|ECG.Interval.QRS^QRS interval^MESI_MRECORDS||0|ms^Milliseconds^UCUM|||||F
OBX|7|NM|ECG.Interval.QTC^QTC interval^MESI_MRECORDS||0|ms^Milliseconds^UCUM|||||F
OBX|8|NM|ECG.Axis.P^P Axis Value^MESI_MRECORDS||0|°^Degrees^UCUM|||||F
OBX|9|NM|ECG.Axis.QRS^QRS Axis Value^MESI_MRECORDS||0|°^Degrees^UCUM|||||F
OBX|10|NM|ECG.Axis.AT^T Axis Value^MESI_MRECORDS||0|°^Degrees^UCUM|||||F
OBX|11|NM|ECG.Axis.PII^PII Axis Value^MESI_MRECORDS||0.0|mV^Millivolts^UCUM|||||F
OBX|12|NM|ECG.Axis.S(V1)^S(V1) Axis Value^MESI_MRECORDS||0.0|mV^Millivolts^UCUM|||||F
OBX|13|NM|ECG.Axis.R(V5)^R(V5) Axis Value^MESI_MRECORDS||0.0|mV^Millivolts^UCUM|||||F
OBX|14|NM|ECG.Axis.Sokol^Sokol Axis Value^MESI_MRECORDS||0.0|mV^Millivolts^UCUM|||||F
OBX|15|NM|ECG.Meta.HR^Patient heart rate^MESI_MRECORDS||65|bpm^Beats per minute^UCUM|||||F

ℹ sending ACK message
› MSH|^~\&|||||||ACK|MSG5213843||2.5.1
MSA|AA|MSG5213843


 ERROR  client error read ECONNRESET

  at TCP.onStreamRead (internal/stream_base_commons.js:183:27)

ℹ client connection closed
```



## FAQ actual

### How to remove message from work list

* WL receives order from server
* the name is displayed in `Working list`
* user doesn't want to perform EKG
* how is it possible to cancel the request on mTablet screen ?
  
### How to use demo mode to perform demo EKG

* logout on mTablet
* open main menu (cog right down corner)
* find item `General/Application version/Current version`
* long press on it
* Simulation preferences will appear
* enable `ecg hw sim`
* enable `Demonstration status`
* login as admin user to mTablet
* open `Work list application` and select user
* `START` button is now available

The problem is it never simulates any data.

### What is the process to enable Work list application on users mTablet

Default state of mTablet is, that it doesn't contain the Work list application. 

### Why are patients data still uploaded 

Patients added to work list appear also on https://mrecords.mesimedical.com/.
How to disable this ? 

